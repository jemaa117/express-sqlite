## TP3 - Docker, Docker Compose et CI/CD
### Objectives:
Objective of the tutorial: The aim of this tutorial is to guide you through the process of creating, containerizing and deploying an Express.js application with SQLite using Docker.

containerization and deployment of an Express.js application with SQLite using Docker
and Docker Compose.

You'll also learn how to automate the CI/CD pipeline using Jenkins to create the the image

## Step 1: Project configuration on GitLab
    
1. Create a project on GitLab (or GitHub) with the title "express-sqlite".
2. Clone the project on your computer.
3. Set up a GitFlow workflow for the project using the tool of your choice.

## Step 2: Creating the Express.js application with SQLite
1. Access the directory for your application:
```bash
cd express-sqlite
```
2. Initialize a Node.js project:
```
npm init   
```
3. Install the necessary dependencies (Express.js and SQLite):
```bash
npm install express sqlite3
```
4. Create an app.js file and add the following Express.js application code.
Make sure the application works properly by testing it with Postman.
```bash
const express = require('express');
const sqlite3 = require('sqlite3');
const app = express();
const port = 3000;
const db = new sqlite3.Database('mydatabase.db');

db.serialize(() => {
    db.run('CREATE TABLE IF NOT EXISTS items (id INTEGER PRIMARY KEY, name TEXT)');
});
app.use(express.json());
app.get('/items', (req, res) => {
    db.all('SELECT * FROM items', (err, rows) => {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }
        res.json({ items: rows });
    });
});
app.post('/items', (req, res) => {
    const { name } = req.body;
    db.run('INSERT INTO items (name) VALUES (?)', [name], function (err) {
        if (err) {
            res.status(500).json({ error: err.message });
            return;
        }
        res.json({ id: this.lastID });
    });
});
app.listen(port, () => {
    console.log(`Server working on ${port}`);
});
```

## Step 3: Creating the Docker image
1. Create a Dockerfile in the project's root directory:

```bash
FROM node:lts-alpine
WORKDIR /usr/src/app
COPY package*.json ./
COPY app.js ./
RUN npm install
EXPOSE 3000
CMD ["node", "app.js"]
```
2. Create the Docker image using the following command:
```bash 
docker build -t <username>/express-sqlite-app .
```
3. Test the Docker image locally by starting it in a container:
```bash 
docker run -p 3000:3000 <username>/express-sqlite-app
```
NB: Check that the application is running by accessing http://localhost:3000.

4. Connect to DockerHub and publish the Docker image created with the command :

NB: To connect to your DockerHub Account try :
```bash 
Docker login 
```
```bash 
docker push <username>/express-sqlite-app
```

## Step 4: Creating Docker Compose
1. Create a docker-compose.yml file to define the Docker Compose configuration.

Use the following example, modifying it as appropriate:
```bash 
version: '3'
services:
   app:
     image: express-sqlite-app
     container_name: <username>/express-sqlite-app
     ports:
        - "3000:3000"
   database:
     image: esolang/sqlite3:latest
     container_name: sqlite-db
     volumes:
        - ./mydatabase.db:/usr/src/app/mydatabase.db
```

2. Run the application using Docker Compose :
```bash 
docker-compose up
```

NB: Make sure the application and database are working properly. 

## Step 5: Creating the Jenkinsfile

1. Add your DockerHub login to Jenkins by following this guide: https://www.jenkins.io/doc/book/using/using-credentials/

NB: Your login must have the ID: DOCKERHUB_CREDENTIALS

2. Create a Jenkinsfile at the root of your project with the following contents:
```bash
pipeline {
    agent any
    environment {
        // Ajouter la variable dh_cred comme variables d'authentification
        DOCKERHUB_CREDENTIALS = credentials('medhedijemaa-dockerhub')
    }
    stages {
        stage('Checkout'){
            agent any
            steps{
                // Changer avec votre lien gitlab/github
                git branch: 'main', url:'https://gitlab.com/jemaa117/express-sqlite.git'
            }
        }
        stage('Init'){
            steps{
                // Permet l'authentification
                sh 'echo $DOCKERHUB_CREDENTIALS_PSW | docker login -u $DOCKERHUB_CREDENTIALS_USR --password-stdin'
            }
        }
        stage('Build'){
            steps {
                //Changer "username" avec votre username sur DockerHub
                sh 'docker build -t medhedijemaa/express-sqlite-app:latest -f ./Dockerfile .'
            }
        }
        stage('Deliver'){
            steps {
                //Changer "username" avec votre username sur DockerHub
                sh 'docker push medhedijemaa/express-sqlite-app:latest'
            }
        }
        stage('Cleanup'){
            steps {
            //Changer "username" avec votre username sur DockerHub
            sh 'docker rmi medhedijemaa/express-sqlite-app:latest'
            sh 'docker logout'
            }
        }
    }
}
```

3. Make the necessary commits and pushes and check the execution of your Pipeline.